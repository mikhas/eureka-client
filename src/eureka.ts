export interface EurekaTransport {
    register(instance: InstanceInfo): Promise<OperationResult>

    deregister(appId: string, instanceId: string): Promise<OperationResult>

    heartbeat(appId: string, instanceId: string): Promise<OperationResult>

    instances(): Promise<ApplicationInfo[]>

    appInstances(appId: string): Promise<InstanceInfo>

    appInstance(appId: string, instanceId: string): Promise<InstanceInfo>
    
    instance(instanceId: string): Promise<InstanceInfo>

    setStatus(appId: string, instanceId: string, status: StatusType): Promise<OperationResult>

    setMetadata(appId: string, instanceId: string, key: string, value: string): Promise<OperationResult>
}

export interface ApplicationInfo {
    name: string
    instance: InstanceInfo[]
}

export interface InstanceInfo {
    app: string
    instanceId?: string
    hostName?: string
    ipAddr?: string
    vipAddress?: string
    secureVipAddress?: string
    status?: StatusType
    port?: number | Port
    securePort?: number | Port
    homePageUrl?: string
    statusPageUrl?: string
    healthCheckUrl?: string
    dataCenterInfo?: DataCenterInfo
    leaseInfo?: LeaseInfo
    metadata?: InstanceMetadata
}

export interface InstanceMetadata {
    [key: string]: string
}

export interface LeaseInfo {
    evictionDurationInSecs?: number
}

export const DEFAULT_DATA_CENTER_INFO_CLASS = 'com.netflix.appinfo.InstanceInfo$DefaultDataCenterInfo'

export interface DataCenterInfo {
    "@class": string | 'com.netflix.appinfo.InstanceInfo$DefaultDataCenterInfo'
    name: string | "MyOwn" | "Amazon"
    metadata?: AmazonMetadata
}

export interface AmazonMetadata {
    "ami-launch-index": string
    "local-hostname": string
    "availability-zone": string
    "instance-id": string
    "public-ipv4": string
    "public-hostname": string
    "ami-manifest-path": string
    "local-ipv4": string
    "hostname": string
    "ami-id": string
    "instance-type": string
}

export interface Port {
    "$": number,
    "@enabled": boolean
}

export enum StatusType {
    UP = 'UP',
    DOWN = 'DOWN',
    STARTING = 'STARTING',
    OUT_OF_SERVICE = 'OUT_OF_SERVICE',
    UNKNOWN = 'UNKNOWN'
}

export interface OperationResult {
    success: boolean
    message?: string
}