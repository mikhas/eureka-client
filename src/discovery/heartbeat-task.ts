import * as eureka from 'eureka'

export class HeartbeatTask {
    private transport: eureka.EurekaTransport
    private instanceInfo: eureka.InstanceInfo
    private timer: number

    constructor(transport: eureka.EurekaTransport, instanceInfo: eureka.InstanceInfo, heartbeatInterval: number){
        this.transport = transport
        this.instanceInfo = instanceInfo
        this.initHeartbeatTask(heartbeatInterval)
    }

    private initHeartbeatTask(interval: number){
        console.info('Starting heartbeat task')
        this.sendHeartbeat().then(()=>{
            this.timer = setInterval(this.sendHeartbeat.bind(this), interval * 1000)
        })
    }

    private sendHeartbeat(){
        return this.transport.heartbeat(this.instanceInfo.app, this.instanceInfo.instanceId).then(()=>{
            console.info('Heartbeat sent')
        }, (err)=> {
            console.error('error while sending heartbeat', err)
        })
    }

    public stop(){
        console.info('Stopping heartbeat task')
        clearInterval(this.timer)
    }
}