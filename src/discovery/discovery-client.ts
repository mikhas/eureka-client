import * as eureka from "eureka";
import { setInterval } from "timers";
import { StatusType, DEFAULT_DATA_CENTER_INFO_CLASS } from "eureka";
import { ServiceRegistry } from "discovery/service-registry";
import { HeartbeatTask } from "discovery/heartbeat-task";

export class DiscoveryClient {

    private instanceInfo: eureka.InstanceInfo
    private configuration: DiscoveryClientConfig
    private serviceRegistry: ServiceRegistry
    private heartbeatTask: HeartbeatTask
    private transport: eureka.EurekaTransport

    constructor(instance: eureka.InstanceInfo, config: DiscoveryClientConfig, transport: eureka.EurekaTransport){
        this.instanceInfo = adjustInstanceInfo(instance)
        this.configuration = adjustConfiguration(config)
        this.transport = transport
        
        if(this.configuration.fetchRegistry){
            this.serviceRegistry = new ServiceRegistry(transport, this.configuration.registryFetchInterval)
        }

        if(this.configuration.registerWithEureka) {
            transport.register(this.instanceInfo).then(()=>{
                this.heartbeatTask = new HeartbeatTask(transport, this.instanceInfo, this.configuration.heartbeatInterval)
            })
        }
        
    }

    getApplication(appId: string): eureka.InstanceInfo[]{
        return this.configuration.fetchRegistry
            ? this.serviceRegistry.getApplicationInstances(appId)
            : []
    }

    getApplications(): eureka.InstanceInfo[] {
        return this.configuration.fetchRegistry
        ? this.serviceRegistry.getInstances()
        : []
    }

    stop(){
        if(this.serviceRegistry)
            this.serviceRegistry.stop()
        if(this.heartbeatTask)
            this.heartbeatTask.stop()
        
        return this.transport.deregister(this.instanceInfo.app, this.instanceInfo.instanceId)        
    }
}

function adjustConfiguration(config: DiscoveryClientConfig): DiscoveryClientConfig {
    const dcc: DiscoveryClientConfig = {}
    dcc.registerWithEureka = config.registerWithEureka === undefined ? true : config.registerWithEureka
    dcc.fetchRegistry = config.fetchRegistry === undefined ? true : config.fetchRegistry
    dcc.heartbeatInterval = config.heartbeatInterval || 30
    dcc.registryFetchInterval = config.registryFetchInterval || 30 
    return dcc
}

function adjustInstanceInfo(source: eureka.InstanceInfo): eureka.InstanceInfo {
    const info: eureka.InstanceInfo = {
        app: source.app
    }
    
    info.hostName = source.hostName || process.env.HOSTNAME || process.env.COMPUTERNAME
    info.status = source.status || StatusType.UP
    info.instanceId = source.instanceId || info.hostName
    info.port = !source.port ? {
        "@enabled": true,
        $: process.env.PORT ? parseInt(process.env.PORT) : 8080
    } : (
        typeof source.port === 'number' ? {
            "@enabled": true,
            $: source.port
        } :
        source.port
    )

    if(source.securePort) {
        info.securePort = typeof source.securePort === 'number' ? {
            "@enabled": true,
            $: source.securePort
        } :
        source.securePort
    }

    info.dataCenterInfo = source.dataCenterInfo || {
        "@class": DEFAULT_DATA_CENTER_INFO_CLASS,
        name: "MyOwn"
    }

    info.leaseInfo = source.leaseInfo || {
        evictionDurationInSecs: 30
    }

    info.ipAddr = source.ipAddr || "127.0.0.1"

    info.healthCheckUrl = source.healthCheckUrl
    info.homePageUrl = source.homePageUrl
    info.statusPageUrl = source.statusPageUrl
    info.vipAddress = source.vipAddress
    info.secureVipAddress = source.secureVipAddress

    return info
}

export interface DiscoveryClientConfig {
    fetchRegistry?: boolean
    registerWithEureka?: boolean
    registryFetchInterval?: number
    heartbeatInterval?: number
}