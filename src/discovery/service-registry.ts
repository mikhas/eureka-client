import * as eureka from "eureka";

export class ServiceRegistry {
    
        private transport: eureka.EurekaTransport
        private timer: number
        private cache: Map<string, eureka.InstanceInfo[]>
        
        constructor(transport: eureka.EurekaTransport, registryFetchInterval: number){
            this.transport = transport
            this.cache = new Map()
            this.initRegistryFetchTask(registryFetchInterval)
        }
    
        private initRegistryFetchTask(interval: number){
            console.info('Starting registry fetch task')
            this.fetchAndCacheRegistry().then(()=>{
                this.timer = setInterval(this.fetchAndCacheRegistry.bind(this), interval * 1000)
            })
        }
    
        private fetchAndCacheRegistry(): Promise<any> {
            console.info('Fetching registry')
            return this.transport.instances()
                .then(this.updateCache.bind(this), (err)=>{
                    console.error('error while fetching registry', err)
                })
        }
    
        private updateCache(applications: eureka.ApplicationInfo[]){
            console.info('Updating registry cache')
            const newCache = new Map<string, eureka.InstanceInfo[]>()
            applications.forEach(app => newCache.set(app.name, app.instance))
            this.cache = newCache
        }
    
        getApplicationInstances(appId: string): eureka.InstanceInfo[]{
            const instances = this.cache.get(appId)
            return instances || []
        }
    
        getInstances(): eureka.InstanceInfo[]{
            let allInstances = []
            for(let [app, instances] of this.cache){
                allInstances.push(... instances)
            }
            return allInstances
        }
    
        stop() {
            console.info('Stopping registry fetch task')
            clearInterval(this.timer)
        }
    }