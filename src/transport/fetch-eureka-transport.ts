import fetch from 'node-fetch'
import { Response } from 'node-fetch'
import * as eureka from 'eureka';

const ACCEPT_JSON_OPTIONS = {headers:{"Accept":'application/json'}}

export class FetchEurekaTransport implements eureka.EurekaTransport {

    private baseUrl: string

    constructor(baseUrl: string){
        this.baseUrl = baseUrl
    }

    register(instance: eureka.InstanceInfo): Promise<eureka.OperationResult> {
        return fetch(`${this.baseUrl}/apps/${instance.app}`,{
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({instance})
        }).then(handleStatus(204))
    }
    deregister(appId: string, instanceId: string): Promise<eureka.OperationResult> {
        return fetch(`${this.baseUrl}/apps/${appId}/${instanceId}`,{method: 'DELETE'}).then(handleStatus())
    }
    heartbeat(appId: string, instanceId: string): Promise<eureka.OperationResult> {
        return fetch(`${this.baseUrl}/apps/${appId}/${instanceId}`,{method: 'PUT'}).then(handleStatus())
    }
    instances(): Promise<eureka.ApplicationInfo[]> {
        return fetch(`${this.baseUrl}/apps`, ACCEPT_JSON_OPTIONS)
            .then(handleJson())
            .then((res: any) => res.applications.application)
    }
    appInstances(appId: string): Promise<eureka.InstanceInfo> {
        return fetch(`${this.baseUrl}/apps/${appId}`, ACCEPT_JSON_OPTIONS)
        .then(handleJson())
        .then((res: any) => res.application.instance)
    }
    appInstance(appId: string, instanceId: string): Promise<eureka.InstanceInfo> {
        return fetch(`${this.baseUrl}/apps/${appId}/${instanceId}`, ACCEPT_JSON_OPTIONS)
        .then(handleJson())
        .then((res: any) => res.instance)
    }
    instance(instanceId: string): Promise<eureka.InstanceInfo> {
        return fetch(`${this.baseUrl}/instances/${instanceId}`, ACCEPT_JSON_OPTIONS)
        .then(handleJson())
        .then((res: any) => res.instance)
    }
    setStatus(appId: string, instanceId: string, status: eureka.StatusType): Promise<eureka.OperationResult> {
        return fetch(`${this.baseUrl}/apps/${appId}/${instanceId}/status?value=${status}`,{
            method: 'PUT'
        }).then(handleStatus())
    }
    setMetadata(appId: string, instanceId: string, key: string, value: string): Promise<eureka.OperationResult> {
        return fetch(`${this.baseUrl}/apps/${appId}/${instanceId}/metadata?${key}=${value}`,{
            method: 'PUT'
        }).then(handleStatus())
    }

}

function handleStatus(successStatus: number = 200): (response: Response) => Promise<eureka.OperationResult> {
    return (response: Response) => {
        return new Promise((resolve, reject)=>{
            if(response.status == successStatus){
                    resolve({success:true});
            } else if (response.status == 404) {
                reject({success: false, message: response.url})
            } else {
                reject({success: false, message: response.statusText})
            }
        });
    };
}

function handleJson(successStatus: number = 200): (response: Response) => Promise<any>{
    return (response: Response) => {
        return new Promise((resolve, reject)=>{
            if(response.status == successStatus){
                response.json().then(resolve);
            } else if (response.status == 404) {
                reject({success: false, message: response.url})
            } else {
                reject({success: false, message: response.statusText})
            }
        });
    };
}