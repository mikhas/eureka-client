node-eureka-client
==================

An **IN DEVELOPMENT** javascript/node.js client for [Netflix Eureka](https://github.com/Netflix/eureka)

Installation
------------

```shell
npm install node-eureka-client --save
```

Usage
-----

```javascript
import * as eureka from 'node-eureka-client'

const instanceInfo = {
    appId: 'my-test-applicaton'
    /* ... more config ... */
}

const clientConfig = {
    fetchRegistry: true
    registerWithEureka: true
    registryFetchInterval: 30
    heartbeatInterval: 30
}

const transport = eureka.FetchEurekaTransport('http://<<eureka-server>>:<<port>>/<<base>>') // node-fetch based transport

const client = new eureka.DiscoveryClient(instanceInfo, clientConfig, transport)

// wait a bit

client.getApplication('MY-APP').forEach(instanceInfo => {
    console.info(`found ${instanceInfo.instanceId}`)
})

// after you finish
client.stop()
```